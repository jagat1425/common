<?php  global $page_id;  ?>
<?php $page_id = get_queried_object_id(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<?php if (is_search()):  ?>
        <meta name="robots" content="noindex, nofollow" /> 
	<?php endif; ?>
	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/import.css" />
    <link rel='stylesheet' id='style-css'  href='<?php bloginfo('template_url'); ?>/css/diapo.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style2.css" type="text/css" media="screen"/>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
    
    <script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.min.js'></script>
    <script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js'></script> 
    <script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/jquery.hoverIntent.minified.js'></script> 
    <script type='text/javascript' src='<?php bloginfo('template_url'); ?>/js/diapo.js'></script> 
    <script>
    $(document).ready(function()
    {
    	// Mian Slider Scripts Starts //
        $('.pix_diapo').diapo(
        {
    		loaderColor			: '#fff',
    		navigation			: false
        });
        
        /*** Header Search Scripts Starts */
        var $ui 		= $('#ui_element');
        $ui.find('.sb_input').bind('focus click',function()
        {
            $ui.find('.sb_down')
            .addClass('sb_up')
            .removeClass('sb_down')
            .andSelf()
            .find('.sb_dropdown')
            .show();
        });
        
        /**
        * on mouse leave hide the dropdown, 
        * and change the arrow image
        */
        $ui.bind('mouseleave',function()
        {
            $ui.find('.sb_up')
            .addClass('sb_down')
            .removeClass('sb_up')
            .andSelf()
            .find('.sb_dropdown')
            .hide();
        });
        
        /**
        * selecting all checkboxes
        */
        $ui.find('.sb_dropdown').find('label[for="all"]').prev().bind('click',function(){
        $(this).parent().siblings().find(':checkbox').attr('checked',this.checked).attr('disabled',this.checked);
        });
    });
    </script>
    <?php //wp_head(); ?>	
</head>
<body <?php body_class(); ?>>
	<div class="wrapper">
    	<div class="wrapper-container">
        	<div class="header">
            	<div class="logo">
           	    <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" width="586" height="111" alt="company logo" /> </a>
                </div>
            	<div class="header-right">
                		<img src="<?php bloginfo('template_url'); ?>/images/telephone_icon.png" width="40" height="40" alt="call" />
<h1>01-4436746</h1>
                        <form id="ui_element" class="sb_wrapper">
                    <p>
						<span class="sb_down"></span>
						<input class="sb_input" type="text" value="search your items" onclick="if(this.value == 'search your items'){this.value= ''}" onblur="if(this.value == ''){this.value ='search your items'}"/>
						<input class="sb_search" type="submit" value=""/>
					</p>
					<ul class="sb_dropdown" style="display:none;">
						<li class="sb_filter">Filter your search</li>
						<li><input type="checkbox" id="all"/><label for="all"><strong>All Categories</strong></label></li>
						<li><input type="checkbox" id="Tandoor"/><label for="Tandoor">Tandoor</label></li>
						<li><input type="checkbox" id="Pots"/><label for="Pots">Pots</label></li>
						<li><input type="checkbox" id="Flower Vast"/><label for="Flower Vast">Flower Vast</label></li>
						<li><input type="checkbox" id="Buddha"/><label for="Buddha">Buddha</label></li>
						<li><input type="checkbox" id="Water Fountain"/><label for="Water Fountain">Water Fountain</label></li>
						<li><input type="checkbox" id="Lights"/><label for="Lights">Lights</label></li>
						<li><input type="checkbox" id="Glased Items"/><label for="Glased Items">Glased Items</label></li>
						<li><input type="checkbox" id="Gift Items"/><label for="Gift Items">Gift Items</label></li>
						<li><input type="checkbox" id="Decoration Items"/><label for="Decoration Items">Decoration Items</label></li>
						<li><input type="checkbox" id="Miscellaneous"/><label for="Miscellaneous">Miscellaneous</label></li>
					</ul>
                </form>
                </div>
                <div class="clr"></div>
			 </div>
       		
        	<div class="nav">
        	
            	<!--<li class="first"><a href="index.html" class="active">HOME</a></li>
                <li><a href="newproduct.html">NEW PRODUCT</a></li>
                <li><a href="specials.html">SPECIALS</a></li>
                <li><a href="allproducts.html">ALL PRODUCTS</a></li>
                <li class="last"><a href="contactus.html">CONTACT US</a></li>-->
                <?php dynamic_sidebar('sidebar-menu'); ?>
            
            </div>

        