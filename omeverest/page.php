<?php get_header(); ?>

<div class="container">
    <div class="side-bar left">
    	<h3>CATEGORIES</h3>
        <ul>
        	<li><a href="#">Tandoor</a></li>
            <li><a href="#">Pots</a></li>
            <li><a href="#">Flower Vast</a></li>
            <li><a href="#">Buddha</a></li>
            <li><a href="#">Water Fountain</a></li>
            <li><a href="#">Lights</a></li>
            <li><a href="#">Gladge Items</a></li>
            <li><a href="#">Gift Items</a></li>
            <li><a href="#">Decoration Items</a></li>
            <li><a href="#">miscellaneous</a></li>
        </ul>
    </div>
    <div class="main right">
        <?php query_posts("page_id=$page_id");
        if (have_posts()) : ?>
        <div>
        <?php while (have_posts()) : the_post(); ?>
        <?php echo the_content('Continue reading �'); endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
