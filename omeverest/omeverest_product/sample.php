<?php
/*
Plugin Name: OmEverest Product
Plugin URI: http://jagatkp.com.np
Description: This is a Plugin for displaying product of OmEverest Ceramic.
Author: Jagat Krishna Prajapati
Author URI: http://jagatkp.com.np
Version: 1.0
*/

add_shortcode('omeve_product',function($atts,$content){    // first one is shortcode name and 2nd is function. function is 
    
    //return 'sample  plugin test'; // do not echo here, we need to return 
    //print_r($attribute);
    
    $return_content = '';
    if(isset($atts['cat_id'])){
        
        $atts = shortcode_atts(
            array(
                'cat_id'    =>$atts['cat_id'],
                'content'   =>!empty($content)?$content:''
            ),$atts);
            
        extract($atts);
        
        $args = array( 'category' => $cat_id);
        $myposts = get_posts( $args );        
        foreach( $myposts as $post ) : setup_postdata($post); 
        $return_content .= '
        <div class="product_box">'.
        get_the_content()
        .'</div>';
         endforeach; 
    }
    else{
        $return_content = 'Please use [omeve_product cat_id=Your_Cat_ID]';
    }
    return $return_content;
    
    
}); 

add_action('init', function(){
    $labels = array(    
    'menu_name' => _x('Omeve Product', 'omeve_product') //first one is menu name displayed in backend menu and 2nd one is post_type we can see in wp_post table in db
    
    );
    
    $args = array(
    
    'labels' => $labels,
    
    'hierarchical' => true,
    
    'description' => 'This is product list description',
    
    'supports' => array('title', 'editor'),
    
    'public' => true,
    
    'show_ui' => true,
    
    'show_in_menu' => true,
    
    'show_in_nav_menus' => true,
    
    'publicly_queryable' => true,
    
    'exclude_from_search' => false,
    
    'has_archive' => true,
    
    'query_var' => true,
    
    'can_export' => true,
    
    'rewrite' => true,
    
    'capability_type' => 'post'
    
    );
    
    register_post_type('omeve_product', $args);	   
});

add_action('add_meta_boxes',function(){
    
        add_meta_box("omeve-product-list", "Product Detail", 'omeve_product_list_box', "omeve_product", "normal"); // first one is unique key for meta box, 2nd is meta title, 3rd is to execute the meta boxes and 4th  is the unique post type we created earlier and last one is normal as default
});

function omeve_product_list_box(){
    global $post;
    
    
       $product_details = get_post_meta($post->ID, "_omeve_product", true);
	    //print_r($product_details);exit;
	    $product_details = ($product_details != '') ? json_decode($product_details) : array();
    
       // Use nonce for verification
	    $html =  '<input type="hidden" name="omeve_product_list_box_nonce" value="'. wp_create_nonce(basename(__FILE__)). '" />';
	 
	    
        $html .= '
	<table class="form-table">
	<tbody>
	<tr>
	<th><label for="Product Image">Product Image</label></th>
	<td><input id="omeve_product_upload" type="text" name="product[]" value="'.$product_details[0].'" /></td>
	</tr>
	<tr>
	<th><label for="Upload Images">Product Name</label></th>
	<td><input id="omeve_product_upload" type="text" name="product[]" value="'.$product_details[1].'" /></td>
	</tr>
	<tr>
	<th><label for="Upload Images">Product Price</label></th>
	<td><input id="omeve_product_upload" type="text" name="product[]" value="'.$product_details[2].'" /></td>
	</tr>
	</tr>
	<th><label for="Upload Images">Currency</label></th>
	<td><input id="omeve_product_upload" type="text" name="product[]" value="'.$product_details[3].'" /></td>
	</tr>	
	</tbody>
	</table>
	';
	 
	        echo $html;
    
}
add_action('save_post', function($post_id){
    
    // verify nonce    
    if (!wp_verify_nonce($_POST['omeve_product_list_box_nonce'], basename(__FILE__))) {
    
    return $post_id;
    
    
    }
    
    // check autosave
    
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    
    return $post_id;
    
    }
    
    // check permissions
    
    if ('omeve_product' == $_POST['post_type'] && current_user_can('edit_post', $post_id)) {
    
    //print_r($_POST['product']); echo ' post id => '.$post_id; exit;
    
    $product_details = (isset($_POST['product']) ? $_POST['product'] : '');
    
    $product_details = strip_tags(json_encode($product_details));
    
    update_post_meta($post_id, "_omeve_product", $product_details);
    
    } else {
    
    return $post_id;
    
    }
});
	 
	
	 

	 
		
