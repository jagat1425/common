<?php
/**
 * Template Name: Product Page Layout
 * Description: This is product page layout
 *
 * The showcase template in Twenty Eleven consists of a featured posts section using sticky posts,
 * another recent posts area (with the latest post shown in full and the rest as a list)
 * and a left sidebar holding aside posts.
 *
 * We are creating two queries to fetch the proper posts and a custom widget for the sidebar.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
get_header(); ?>
<div class="new_product">
    <div class="product_title">
        <h3>
        <?php     
        $cats = get_the_category();
        echo $cat_name = $cats[0]->name;   
        ?>
        </h3>
        <ul>
            <li><a href="#" class="no_underline">&lt; &lt; Previous &gt; &gt;</a></li>
            <li><a href="#" class="active">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#" class="no_underline">&lt; &lt; Next &gt; &gt;</a></li>
        </ul>
    </div>


    
    
    <?php
    //$category = get_the_category();
    //$category_id = $category->cat_ID; 
 
    
    $category_id =  get_query_var('cat');
    $args = array( 'category' => $category_id);
    $myposts = get_posts( $args );
    foreach( $myposts as $post ) : setup_postdata($post); ?>
    <div class="product_box">
    <?php the_content(); ?>
    <?php the_meta(); ?>
    </div>
    <?php endforeach; ?> 
    <div class="product_box">
        <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/feature1.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <!--<div class="product_box">
        <a href="#"><img class="items" src="images/royal-blue-clay-pinch-bo.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/royal-blue-clay-pinch-bo.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/royal-blue-clay-pinch-bo.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/fish.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/fish.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/fish.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/fish.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/feature3.jpg" width="196" height="226" alt="item1" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/feature3.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/feature3.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/feature3.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/royal-blue-clay-pinch-bo.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/royal-blue-clay-pinch-bo.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>
    <div class="product_box">
        <a href="#"><img class="items" src="images/royal-blue-clay-pinch-bo.jpg" width="196" height="226" alt="items" /></a>
        <h4>Product Name</h4>
        <p>Rs.4500</p>
        <span><a href="#">Details</a></span>
    </div>-->
</div> 
<?php get_footer(); ?>