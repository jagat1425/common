			<div class="footer">
            	<div class="footer-left">
                <h5>Links</h5>
                <?php dynamic_sidebar('sidebar-menu'); ?>
                </div>
                <div class="footer-mid">
               	<h5>Connect Us</h5>
               	  <ul>
           			  <li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/facebook.png" width="24" height="24" alt="facebook_icon" />Facebook</a></li>
                      <li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png" width="24" height="24" alt="twitter_icon" />Twitter</a></li>
                      <li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/youtube.png" width="24" height="24" alt="youtube_icon" />Youtube</a></li>
                 </ul>
                </div>
                <div class="footer-right">
                 	<h5>Contact Us</h5>
                    <p>
                    	Om Everest Ceramic<br />
                        Kamal Pokhari,Kathmandu<br />
                        Nepal<br />
                     	Telephone: 01-4436746
                    </p>
                    </div>
                <div class="clr"></div>
            	<div class="copy_right">
                	<p>&copy; Om Everest Ceramic,2013. All rights reserved.VAT registration number 111 1111 11. PAN no. 111 1111 11</p>
                </div>
            </div>        
         </div>
    </div>
	<?php #wp_footer(); ?>
	<!-- Don't forget analytics -->
</body>
</html>