<?php get_header(); ?>
<div class="banner-container">
        <div class="banner">
                <section>
                <div class="pix_diapo">
                    <div data-thumb="<?php bloginfo('template_url'); ?>/images/mainbanner/wall-items.jpg">
                        <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/mainbanner/wall-items.jpg" width="625" height="390" /></a>
                    </div>
                    <div data-thumb="<?php bloginfo('template_url'); ?>/images/mainbanner/tandoori-oven.jpg">
                        <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/mainbanner/tandoori-oven.jpg" width="625" height="390" /></a>
                    </div>
                    <div data-thumb="<?php bloginfo('template_url'); ?>/images/mainbanner/new-flower-vase.jpg">
                        <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/mainbanner/new-flower-vase.jpg" width="625" height="390" /></a>
                    </div>
                    <div data-thumb="<?php bloginfo('template_url'); ?>/images/mainbanner/water-fountain.jpg">
                        <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/mainbanner/water-fountain.jpg" width="625" height="390" /></a>
                    </div>
                    </div>
    			</section>
                 </div>
                <div class="banner-right">
                    
                    
                <?php
                
                $args = array( 'numberposts' => 2, 'category' => 8 );
                $myposts = get_posts( $args );
                foreach( $myposts as $post ) : setup_postdata($post); ?>
                <div class="banner-box">
                    <?php the_content(); ?>
                    <img class="ribbon" src="<?php bloginfo('template_url'); ?>/images/ribbon3.png" width="111" height="107" alt="new" />              		
                </div>
                <?php endforeach; 
                
                ?>
                	<!--<div class="banner-box">
               	    <a href="#"><img class="new-item" src="<?php //bloginfo('template_url'); ?>/images/new_item_1.jpg" width="306" height="207" alt="new item" /></a> 
       	 			<img class="ribbon" src="<?php //bloginfo('template_url'); ?>/images/ribbon3.png" width="111" height="107" alt="new" />
              		</div>
                    <div class="banner-box">
               	    <a href="#"><img class="new-item" src="<?php //bloginfo('template_url'); ?>/images/new_item_2.jpg" width="306" height="207" alt="new item" /></a> 
       	 			<img class="ribbon" src="<?php //bloginfo('template_url'); ?>/images/ribbon3.png" width="111" height="107" alt="new" />
              		</div> -->
              	</div>
        </div>
       	 
        <div class="container">
        	<div class="side-bar left">
            	<h3>CATEGORIES</h3>
                <ul>                
                    <?php wp_list_categories('child_of=2&title_li='); ?>
                </ul>
            </div>
            <div class="main right">
                <div class="product">
                <h3>FEAURED PRODUCTS</h3>
            	<div class="box">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/feature1.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Round Pot</h4>
                <p>Rs.4500</p>
                <span><a href="#">Details</a></span>
                </div>
                <div class="box">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/feature1.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Clay Tandoor</h4>
                <p>Rs.2500</p>
               <span><a href="#">Details</a></span>
                </div>
                <div class="box">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/feature1.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Tandoor-small</h4>
                <p>Rs.3500</p>
               <span><a href="#">Details</a></span>
                </div>
                <div class="box last">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/feature1.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Tandoor-mid</h4>
                <p>Rs.5500</p>
               <span><a href="#">Details</a></span>
                </div>
                 </div>
                 <div class="clr"></div>
                <div class="product">
                <h3>NEW PRODUCTS</h3>
            	<div class="box">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/new_item_1.jpg" width="160" height="158" alt="items" /></a>
               	<h4>pot</h4>
                <p>Rs.5000</p>
                <span><a href="#">Details</a></span>
                </div>
                <div class="box">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/new_item_2.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Tea Cup</h4>
                <p>Rs.1000</p>
               <span><a href="#">Details</a></span>
                </div>
                <div class="box">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/new_item_3.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Mask</h4>
                <p>Rs.550</p>
               <span><a href="#">Details</a></span>
                </div>
                <div class="box last">
           	    <a href="#"><img class="items" src="<?php bloginfo('template_url'); ?>/images/new_item_4.jpg" width="160" height="158" alt="items" /></a>
               	<h4>Design</h4>
                <p>Rs.650</p>
               <span><a href="#">Details</a></span>
                </div>
                 </div>
            </div>
            <div class="clr"></div>
        </div> 
<?php get_footer(); ?>
 