<?php
/**
 * Template Name: contact page
 * Description: page for contact us page
 *
 * The showcase template in Twenty Eleven consists of a featured posts section using sticky posts,
 * another recent posts area (with the latest post shown in full and the rest as a list)
 * and a left sidebar holding aside posts.
 *
 * We are creating two queries to fetch the proper posts and a custom widget for the sidebar.
 *
 * @package WordPress
 * @subpackage omeverest
 * @since omeverest 1.0
 */
get_header(); ?>
<div class="contact">
    <?php query_posts("page_id=$page_id");
    if (have_posts()) : ?>
    <div>
    <?php while (have_posts()) : the_post(); ?>
    <?php echo the_content('Continue reading �'); endwhile; ?>
    </div>
    <?php endif; ?>
    <div class="clr"></div>
</div>
        
        
        
 <div class="clr"></div>  

<?php get_footer(); ?>