<?php
function file_exists_remote($url) {
 $curl = curl_init($url);
 curl_setopt($curl, CURLOPT_NOBODY, true);
 //Check connection only
 $result = curl_exec($curl);
 //Actual request
 $ret = false;
 if ($result !== false) {
  $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
  //Check HTTP status code
  if ($statusCode == 200) {
   $ret = true;  
  }
 }
 curl_close($curl);
 return $ret;
}

$file_info = file_exists_remote('http://jagatkp.com.np/downloads/bootstrap/css/style.css');

if($file_info){
    echo 'file exists';
}
else{
    echo 'file not exists';
}
?>