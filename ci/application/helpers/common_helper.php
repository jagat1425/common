<?php
function get_upload_directory($date,$attachment_type=NULL){
    //preg_match("^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$^",$date)
    //preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)
    if(preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date) || preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/",$date)){
        
        $dates_without_time = explode(' ',$date);
        $dates = explode('-',$dates_without_time[0]);
        
        $year = $dates[0];
        $month= $dates[1];
        $days = $dates[2];
        //print_r($dates); die;
        //echo $year.' '.$month.' '.$days;die;
        
        # $attachment_type = attachment(default), sample, final,
        if($attachment_type):
             if($attachment_type== 'sample'):
                $dirname = 'samples/'.$year.'/'.$month.'/'.$days;
             elseif($attachment_type== 'final'):
                $dirname = 'finals/'.$year.'/'.$month;
             else:
                $dirname = 'attachments/'.$year.'/'.$month;
             endif;
        else:
            $dirname = 'attachments/'.$year.'/'.$month;
        endif;
                
        $directory = $_SERVER['DOCUMENT_ROOT'].'/project_name/uploads/'.$dirname;
        
        if(!is_dir($directory)){
            mkdir($directory, 0777, true);    
            chmod($directory, 0755);
        }
        
        $return_val = $directory;
    }else{
        $return_val = "Invalid Directory Path. Please check your directory path";
    }
    return $return_val;
}