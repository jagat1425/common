<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

   public function __construct(){
    parent::__construct();
    $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    $this->output->set_header("Pragma: no-cache"); 
        
    }

	public function index()
	{
		$this->load->view('test/index');
	}
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */