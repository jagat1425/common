<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Eng',     39],
          ['Social',  46],
          ['Nep',     48],
          ['A/C',     56],
          ['Opt',     58],
          ['EPH',     64],
          ['Math',    65],
          ['sci',     71]
        ]);

        var options = {
          title: 'My Score',
          backgroundColor: '#ff0',
          is3D :true,
          
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 300px; height: 300px;"></div>
    
    https://developers.google.com/chart/interactive/docs/gallery/piechart
  </body>
</html>
